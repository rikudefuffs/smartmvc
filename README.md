# Introduction

This package contains a very simple MVC framework that helps you develop your application faster. It is based on [this MVC framework](https://www.toptal.com/unity-unity3d/unity-with-mvc-how-to-level-up-your-game-development) originally developed by Eduardo Dias da Costa 

# Setup

Add the following line to the dependencies, in the manifest.json of the project, to install the package:
"com.starworkgc.smartmvc": "https://gitlab.com/rikudefuffs/smartmvc"

Add the following line to the manifest.json of the project (or unit tests won't be displayed in the test runner)
"testables": ["com.starworkgc.smartmvc"],

# Usage

If you're trying the tests: just run them from the Test Runner.
If you're adding the package for development, just look at the scripts used in the example scene

# Feedback

Feedback is much appreciated, just reach out to the authors/mantainers by email or linkedin

Enjoy your testing!

Paolo