# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.6] - 2023-05-27
### Changes
- Removed warnings when an event has no receiver

## [1.0.4] - 2021-05-25
### Fixes
- Fixed events dictionary being shared among all Applications

## [1.0.3] - 2020-06-05
### Fixes
- Fixed events dictionary initialization in builds

## [1.0.2] - 2020-03-22
### Changes
- Optimization (breaking): SmartMVC now uses integers instead of strings for events' indexing
- Awake() is now overridable

### Fixes
- Fixed script execution order bug when interacting with other scripts
- Added safety checks for events dictionary usage

## [1.0.0] - 2019-12-30
### Added
- Added core functionalities of the framework
- Added Tests for core functionalities